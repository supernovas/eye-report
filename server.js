var clusterPoint    = require('./app/db/clusterpoint');
var env             = require('./app/env');

clusterPoint().then(function(cps) {
    env.init(cps);
}, function(err) {
    console.log(err);
});