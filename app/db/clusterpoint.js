var $q          = require('q');
var CONFIG      = require('../config');
var cpsAPI      = require('cps-api');
var cpsConfig   = CONFIG.cps;


module.exports = function() {
    var deferred = $q.defer();
    var cpsConn = new cpsAPI.Connection(cpsConfig.address, cpsConfig.dbName, cpsConfig.email, cpsConfig.password, 'document', 'document/id', {account: cpsConfig.acctId});
    cpsConn.sendRequest(new cpsAPI.StatusRequest(), function (err, status_resp) {
        if (err) {
            deferred.reject(err);
        } else {
            var cps = {
                connection: cpsConn,
                api: cpsAPI
            };
            deferred.resolve(cps);
        }
    }, 'json');

    return deferred.promise;
};
