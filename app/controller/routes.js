/**
 * Created by lawrence on 8/8/2015.
 */

var Officer = require('../service/officer');
var Report  = require('../service/report');
var Citizen = require('../service/citizen');

var Routes = {
    init: function (app, cps) {

        //Officer related API
        app.get('/api/officer/search', function(req, res) {
            var officer = new Officer(req, res, cps);
            officer.searchOfficer();
        });

        app.get('/api/officer/:id', function(req, res) {
            var officer = new Officer(req, res, cps);
            officer.getOfficer();
        });

        app.get('/api/officers', function(req, res) {
            var officer = new Officer(req, res, cps);
            officer.getOfficers();
        });

        app.post('/api/officer', function(req,res) {
            var officer = new Officer(req, res, cps);
            officer.addNewOfficer();
        });

        app.put('/api/officer', function(req, res) {
            var officer = new Officer(req, res, cps);
            officer.updateOfficerDetails();
        });

        app.post('/api/officer/login', function(req, res) {
            var officer = new Officer(req, res, cps);
            officer.login();
        });

        //Incident report related API
        app.post('/api/report', function(req, res) {
            var report = new Report(req, res, cps);
            report.createReport();
        });

        app.get('/api/reports', function(req, res) {
            var report = new Report(req, res, cps);
            report.getReports();
        });

        app.get('/api/report/:id', function(req, res) {
            var report = new Report(req, res, cps);
            report.getReport();
        });

        app.get('/api/reports/citizen/:citizenId', function(req, res) {
            var report = new Report(req, res, cps);
            report.getReportByCitizenId();
        });

        app.get('/api/report/search', function(req, res) {
            var report = new Report(req, res, cps);
            report.searchReport();
        });

        app.put('/api/report', function(req, res) {
            var report = new Report(req, res, cps);
            report.updateReportDetails();
        });

        //Citizen related API
        app.post('/api/citizen/', function(req, res) {
            var citizen = new Citizen(req, res, cps);
            citizen.addNewCitizen();
        });

        app.get('/api/citizens/', function(req, res) {
            var citizen = new Citizen(req, res, cps);
            citizen.getCitizens();
        });

        app.get('/api/citizen/:id', function(req, res){
            var citizen = new Citizen(req, res, cps);
            citizen.getCitizen();
        });

        app.get('api/citizen/search', function(req, res) {
            var citizen = new Citizen(req, res, cps);
            citizen.searchCitizen();
        });

        app.put('api/citizen', function(req, res) {
            var citizen = new Citizen(req, res, cps);
            citizen.updateCitizenDetail();
        });

        app.post('/api/citizen/login', function(req, res) {
            var citizen = new Citizen(req, res, cps);
            citizen.login();
        });

        app.get('*', function (req, res) {
            res.sendFile(process.cwd() + '/public/index.html');
        });
    }
};

module.exports = Routes;