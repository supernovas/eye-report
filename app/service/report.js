/**
 * Created by lawrence on 8/8/2015.
 */

var Report = function(req, res, cps) {
    this.req = req;
    this.res = res;
    this.cps = cps;
};

Report.prototype.createReport = function() {
    var data = this.req.body;

    var newReport = {
        id: 'report' + new Date().getTime(),
        dateCreated: new Date().getTime(),
        status: data.status,
        citizenId: data.citizenId,
        title: data.title,
        longitude: data.longitude,
        latitude: data.latitude,
        incidentType: data.incidentType,
        description: data.description,
        collectionCategory: 'report'
    };

    this.cps.connection.sendRequest(new this.cps.api.InsertRequest(newReport), function (err, res) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Report.prototype.getReports = function() {
    var startDate = this.req.query.startDate;
    var endDate = this.req.query.endDate;
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchString = '<collectionCategory>report</collectionCategory><dateCreated>'+ startDate + ' .. ' +  endDate +'</dateCreated>'
    var searchReq = new this.cps.api.SearchRequest(searchString, offset, total);
    this.sendRequest(searchReq);
};

Report.prototype.getReport = function() {
    var id = this.req.params.id;
    var searchReq = new this.cps.api.SearchRequest('<id>'+ id +'</id>');
    this.sendRequest(searchReq);
};

Report.prototype.getReportByCitizenId = function() {
    var citizenId = this.req.params.citizenId;
    console.log(citizenId);
    var searchReq = new this.cps.api.SearchRequest('<citizenId>'+ citizenId +'</citizenId>');
    this.sendRequest(searchReq);
};

Report.prototype.searchReport = function() {
    var keyword = this.req.query.keyword;
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchReq = new this.cps.api.SearchRequest('{<description>'+ keyword +'</description> ' +
    '<location>'+ keyword +'</location><status>'+ keyword +'</status><location><address>'+ keyword +'</address></location>}', offset, total);
    this.sendRequest(searchReq);
};

Report.prototype.updateReportDetails = function() {
    var updatedReportDetail = this.req.body;
    this.cps.connection.sendRequest(new this.cps.api.UpdateRequest(updatedReportDetail), function (err, resp) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Report.prototype.sendRequest = function(searchReq) {
    this.cps.connection.sendRequest(searchReq, function (err, search_resp) {
        err ? this.res.status(500).json(err) : this.res.json(search_resp.results.document);
    }.bind(this));
};

module.exports = Report;