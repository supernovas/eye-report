/**
 * Created by lawrence on 8/8/2015.
 */

var Citizen = function(req, res, cps) {
    this.req = req;
    this.res = res;
    this.cps = cps;
};

Citizen.prototype.addNewCitizen = function() {
    var data = this.req.body;

    var newCitizen = {
        id: 'report' + new Date().getTime(),
        dateCreated: new Date().getTime(),
        firstName: data.firstName,
        lastName: data.lastName,
        gender: data.gender,
        mobileNumber: data.mobileNumber,
        username: data.username,
        password: data.username,
        email: data.email,
        collectionCategory: 'citizen'
    };

    this.cps.connection.sendRequest(new this.cps.api.InsertRequest(newCitizen), function (err, res) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Citizen.prototype.getCitizens = function() {
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchReq = new this.cps.api.SearchRequest('<collectionCategory>citizen</collectionCategory>', offset, total);
    this.sendRequest(searchReq);
};

Citizen.prototype.getCitizen = function() {
    var id = this.req.params.id;
    var searchReq = new this.cps.api.SearchRequest('<id>'+ id +'</id>');
    this.sendRequest(searchReq);
};

Citizen.prototype.searchCitizen = function() {
    var keyword = this.req.query.keyword;
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchReq = new this.cps.api.SearchRequest('{<firstName>'+ keyword +'</firstName> <lastName>'+ keyword +'</lastName>}', offset, total);

    this.sendRequest(searchReq);
};

Citizen.prototype.login = function() {
    var data = this.req.body;
    var username = data.username;
    var password = data.password;
    var queryString = '<username>'+ username +'</username><password>'+ password +'</password>';
    var searchReq = new this.cps.api.SearchRequest(queryString);
    this.sendRequest(searchReq);
};

Citizen.prototype.updateCitizenDetail = function() {
    var updatedCitizenDetail = this.req.body;
    this.cps.connection.sendRequest(new this.cps.api.UpdateRequest(updatedCitizenDetail), function (err, resp) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Citizen.prototype.sendRequest = function(searchReq) {
    this.cps.connection.sendRequest(searchReq, function (err, search_resp) {
        err ? this.res.status(500).json(err) : this.res.json(search_resp.results.document);
    }.bind(this));
};

module.exports = Citizen;