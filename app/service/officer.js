/**
 * Created by lawrence on 8/8/2015.
 */


var Officer = function(req, res, cps) {
    this.req = req;
    this.res = res;
    this.cps = cps;
};

Officer.prototype.addNewOfficer = function() {
    var data = this.req.body;
    var newOfficer = {
        id: 'officer' + new Date().getTime(),
        username: data.username,
        password: data.password,
        firstName: data.firstName,
        lastName: data.lastName,
        mobileNumber: data.mobileNumber,
        rank: data.rank,
        role: data.role,
        imgUrl: data.imgUrl,
        dateCreated: new Date().getTime(),
        collectionCategory: 'officer'
    };

    this.cps.connection.sendRequest(new this.cps.api.InsertRequest(newOfficer), function (err, res) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Officer.prototype.getOfficers = function() {
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchReq = new this.cps.api.SearchRequest('<collectionCategory>officer</collectionCategory>', offset, total);
    this.sendRequest(searchReq);
};

Officer.prototype.getOfficer = function() {
    var id = this.req.params.id;
    var searchReq = new this.cps.api.SearchRequest('<id>'+ id +'</id>');
    this.sendRequest(searchReq);
};

Officer.prototype.searchOfficer = function() {
    var keyword = this.req.query.keyword;
    var offset = parseInt(this.req.query.offset);
    var total = parseInt(this.req.query.total);
    var searchReq = new this.cps.api.SearchRequest('{<firstName>'+ keyword +'</firstName> <lastName>'+ keyword +'</lastName>}', offset, total);

    this.sendRequest(searchReq);
};

Officer.prototype.updateOfficerDetails = function() {
    var updatedOfficerDetails = this.req.body;
    this.cps.connection.sendRequest(new this.cps.api.UpdateRequest(updatedOfficerDetails), function (err, resp) {
        err ? this.res.status(500).json(err) : this.res.json(200);
    }.bind(this));
};

Officer.prototype.login = function() {
    var data = this.req.body;
    var username = data.username;
    var password = data.password;
    var queryString = '<username>'+ username +'</username><password>'+ password +'</password>';
    var searchReq = new this.cps.api.SearchRequest(queryString);
    this.sendRequest(searchReq);
};

Officer.prototype.sendRequest = function(searchReq) {
    this.cps.connection.sendRequest(searchReq, function (err, search_resp) {
        err ? this.res.status(500).json(err) : this.res.json(search_resp.results.document);
    }.bind(this));
};

module.exports = Officer;