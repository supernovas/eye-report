/**
 * Created by lawrence on 8/8/2015.
 */


/**
 * Created by lawrence on 3/31/2015.
 */

var express         = require('express'),
    app             = express(),
    bodyParser      = require('body-parser'),
    methodOverride  = require('method-override'),
    CONSTANT        = require('./constant'),
    routes          = require('./controller/routes');

var Env = {
    app: app,
    init: function(cps) {
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(methodOverride('X-HTTP-Method-Override'));
        app.use(express.static('./public'));
        app.use(express.static('./uploads'));

        app.use(function(req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            res.setHeader('Access-Control-Allow-Credentials', true);
            next();
        });

        routes.init(app, cps);

        app.listen(CONSTANT.port, function() {
            console.log('Port running on: ' + CONSTANT.port)
        });
    }
};

module.exports = Env;