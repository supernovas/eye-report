angular.module('EyeReport.Monitoring', [])
.config(['$stateProvider',
    function($stateProvider) {
        $stateProvider
            .state('monitoring.list', {
                url: '',
                templateUrl: 'app/components/monitoring/partials/monitoring_list.html',
                controller: 'MonitoringListCtrl'
            })
            .state('monitoring.reports', {
                url: '/reports',
                templateUrl: 'app/components/monitoring/partials/monitoring_report.html'
            })
    }
]);