/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport.Monitoring')
.controller('MonitoringListCtrl', ['$scope', '$state', 'MonitoringListService',
    function($scope, $state, MonitoringListService) {

        $scope.userDetails = [];
        $scope.officers = {};
        $scope.officersByKeyWord = {};
        $scope.keyword = "";
        var offset = "0";
        var total = "10";

        $scope.init = function() {
            $scope.displayOfficers();
        };

        $scope.displayOfficers = function(){
            MonitoringListService.getAllOfficers().then(function (res) {
                $scope.officers = res;
            })
        };

        $scope.getAllOfficersById = function(id) {
            MonitoringListService.getAllOfficersById(id).then(function (res) {
                $scope.userDetails = res[0];
            });
        };

        $scope.getAllOfficersByKeyword = function(keyword) {
            console.log(keyword);
            MonitoringListService.getAllOfficersByKeyword(keyword, offset, total).then(function (res) {
                $scope.officers = res;
            });
        };
    }
]);