/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport')
.factory('MonitoringListFactory', ['$http',
    function($http) {
        return {
            getAllOfficers: function() {
                return($http)({
                    url: '/api/officers',
                    method: 'GET'
                })
            },
            getAllOfficersById: function(id) {
                return($http)({
                    url: '/api/officer/' + id,
                    method: 'GET'
                })
            },
            getAllOfficersByKeyword: function(keyword, offset, total) {
                return($http)({
                    url:'/api/officer/search?keyword=' + keyword + '&offset=' + offset + '&total=' + total,
                    method: 'GET'
                })
            }
        }
    }
]);