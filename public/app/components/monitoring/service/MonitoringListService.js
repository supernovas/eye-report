/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport')
.service('MonitoringListService', ['$q', 'MonitoringListFactory',
    function($q, MonitoringListFactory) {
        this.officers = {};
        this.officerByKeyword = {};
        this.officersById = {};


        this.getAllOfficers = function() {
            var deferred = $q.defer();
            MonitoringListFactory.getAllOfficers().then(function(res){
                this.officers = res.data;
                deferred.resolve(res.data);
            }.bind(this));
            return deferred.promise;
        };

        this.getAllOfficersById = function(id) {
            var deferred = $q.defer();
            MonitoringListFactory.getAllOfficersById(id).then(function(res){
                this.officersById = res.data;
                deferred.resolve(res.data);
            }.bind(this), function(err){
                console.log('err', err);
            });
            return deferred.promise;
        };

        this.getAllOfficersByKeyword = function(keyword, offset, total) {
            var deferred = $q.defer();
            MonitoringListFactory.getAllOfficersByKeyword(keyword, offset, total).then(function(res){
                this.officerByKeyword = res.data;
                deferred.resolve(res.data);
            }.bind(this), function(err){
                console.log('err', err);
            });
            return deferred.promise;
        };
    }
]);