/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport.UI')
    .directive('uiHeader',
    function () {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/ui/partials/header.html'
        }
    })
    .directive('newOfficer', function() {
        return {
            restrict: 'E',
            templateUrl: 'app/shared/user-account/partials/signup.html',
            controller: 'SignUpCtrl'
        }
    })
    .directive('showModal', function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attr) {
                angular.element(elem).click(function () {
                    console.log('click');
                    $('#new-officer').modal('show')
                });
            }
        }
    })
    .directive('closeModal', function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attr) {
                angular.element(elem).click(function () {
                    console.log('click');
                    $('#new-officer').modal('hide')
                });
            }
        }
    });