/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport')
.service('UserService', ['$q', 'UserFactory',
    function($q, UserFactory) {
        this.insertNewOfficer = function(newOfficer) {
            var deferred = $q.defer();
            UserFactory.insertNewOfficer(newOfficer).then(function(res){
                deferred.resolve(res.data);
            }.bind(this), function(err){
                console.log('err', err);
            });
            return deferred.promise;
        }
    }
]);