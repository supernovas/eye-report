/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport.User')
.factory('UserFactory', ['$http',
        function($http) {
            return {
                insertNewOfficer: function(newOfficer) {
                    return $http ({
                        url: '/api/officer',
                        dataType: 'json',
                        method: 'POST',
                        data: newOfficer
                    })
                }
            }
        }
]);