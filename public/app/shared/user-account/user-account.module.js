/**
 * Created by bryan on 8/9/2015.
 */

angular.module('EyeReport.User', [])
.config([ '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('user.sign-up', {
                    url: '/signup',
                    templateUrl: 'app/shared/user-account/partials/signup.html',
                    controller: 'SignUpCtrl'
                })
        }
]);