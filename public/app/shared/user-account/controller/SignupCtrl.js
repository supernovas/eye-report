/**
 * Created by bryan on 8/9/2015.
 */
angular.module('EyeReport')
.controller('SignUpCtrl', ['$scope', 'UserService',
        function($scope, UserService) {
            $scope.newOfficer = {};
            $scope.newOfficer.imgUrl = 'http://intranetfactory.com/viewer/placeholder_440x220.png';

            $scope.insertNewOfficer = function() {
                UserService.insertNewOfficer($scope.newOfficer).then(function(){
                    console.log($scope.newOfficer);
                })
            }
        }
    ]);