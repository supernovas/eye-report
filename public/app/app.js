angular.module('EyeReport',[
    'ui.router',
    'EyeReport.Monitoring',
    'EyeReport.User',
    'EyeReport.UI'
]).config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
    function($urlRouterProvider, $stateProvider, $httpProvider) {
        $stateProvider
            .state('monitoring', {
                url: '/monitoring',
                templateUrl: 'app/components/monitoring/partials/monitoring_main.html',
                abstract: true
            })
            .state('user', {
                url: '/user',
                templateUrl: 'app/shared/user-account/partials/user_main.html',
                abstract: true
            })
    }
]);
